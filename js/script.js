$(function() {
    $("#services").owlCarousel({
        items: 3,
        margin: 20,
        autoplay: true,
        smartSpeed: 700,
        loop: true,
        autoplayHoverPause: true,
        dots: false,
        nav :true,
        navText: ['<i class="lni-chevron-left"></i>','<i class="lni-chevron-right"></i>'],
        // responsive: {
        //     0: {
        //         items: 1
        //     },
        //     480: {
        //         items: 2
        //     },
        //     720: {
        //         items: 2
        //     }
        // }
    });
})

$(window).on('load',function(){
   
    var addressString = "301,Evergreen Chs.,Airoli,Maharashtra,India";
    var myLatLng = {
    lat:19.046605, 
    lng:73.011354
    };
    
    var myMap = new google.maps.Map(document.getElementById("map"),{
        zoom:13,
        center:myLatLng
    });
    
     var custimg = "../img/map/";
    var marker = new google.maps.Marker({
        position:myLatLng,
        map:myMap,
        draggable: true,
        animation: google.maps.Animation.BOUNCE,
        title:"click to see",
        icon: custimg + 'logo.jpg'
    });
    
//    var image = 
    
    var infoWindow = new google.maps.InfoWindow({
       content:addressString 
    });
    marker.addListener('click',function(){
        infoWindow.open(myMap,marker);
    });
    
});
